public class part1
{
	public static void main(String[] args)
	{
		//long可儲存64位元，int只能儲存32，short可儲存16
		//long x = 0;
		short x = 0;
		
		boolean run = true;
		
		while(run)
		{
			x--;
			
			if(x > 0)
				run = false;
		}
		System.out.println(x);		
		
		x++;
		
		System.out.println(x);
	}
}