public class part9
{
	public static void main(String[] args)
	{
		int hit = 0;
		int total = 0;
		
		while(true)
		{
			float x = (float)Math.random() * 2 - 1;		//設定範圍1~-1之間
			float y = (float)Math.random() * 2 - 1;		//設定範圍1~-1之間
			
			//是否有在得分範圍內
			if (x * x + y * y < 1)
			{
				hit++;
			}
			
			System.out.println((float)hit / (float)total * 4.0f + "			[" + total + "]");
			
			total++;
		}
	}
}