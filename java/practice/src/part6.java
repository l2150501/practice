import java.util.Scanner;

public class part6
{
	public static void main(String[] args)
	{
		//猜數字遊戲
		float number = (float)Math.random();		//系統產數字
		number *= 1000.0f;
		
		int number2 = (int)number;
		
		Scanner scanner = new Scanner(System.in);
		
		while(true)
		{
			int guess = scanner.nextInt();		
			
			if(guess > number2)
			{
				System.out.println("Too Large ! !");
			}
			else if(guess < number2)
			{
				System.out.println("Too Small ! !");			
			}
			else
			{
				System.out.println("The Same ! !");
				return;
			}			
		}										
	}	
}